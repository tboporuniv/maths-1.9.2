public class Main {

    public static void main(String[] args) {

        //Can do Scanner, but it is not needed right now
        final int polynomLevel = 9;
        final double  h = 0.0001d, start = 1.92d, end = 2.08d + h;
        final int numOfIters = (int)Math.floor((end - start)/h);
        double[] gornerValues = new double[numOfIters];
        double[] casualValues = new double[numOfIters];
        double[] xValues = new double[numOfIters];
        double[] a_k = new double[] {-512d, 2304d, -4608d, 5376d, -4032d, 2016d, -672d, 144d, -18d, 1d};

        double gornerResult, casualResult;
        int i, j;
        double xCurrentValue = start;
        for(j = 0; j < numOfIters; j++) {
            gornerResult = 0d;
            casualResult = 0d;

            for(i = polynomLevel; i >= 0; i--) {
                gornerResult = gornerResult * xCurrentValue + a_k[i];
            }

            gornerValues[j] = gornerResult;
            System.out.println("p = " + gornerResult + " ;x = " + xCurrentValue + ".\n");

            casualResult = Math.pow((xCurrentValue - 2), 9d);
            casualValues[j] = casualResult;
            System.out.println("p* = " + casualResult + "\n\n\n");

            xValues[i] = xCurrentValue;
            xCurrentValue += h;
        }
    }
}
